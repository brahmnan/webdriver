FROM ubuntu:20.04
RUN apt-get update
RUN apt-get install -y chromium-browser
RUN apt-get install -y python3
RUN apt-get install -y pip
RUN apt-get install -y wget
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update
RUN apt-get install -y google-chrome-stable
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

RUN apt-get autoremove -y && apt-get autoclean

EXPOSE 8000

CMD ["gunicorn", "-w 1", "-b :8000", "main:app"]